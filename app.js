const express = require("express");
const mongan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const produtRoute = require('./api/routes/products');
const orderRoute = require('./api/routes/orders');
const signupRoute = require('./api/routes/user'); 

const app = express();


app.use(mongan('dev'));  // to log every incoming request
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


// CORS Origin error handling just COPY these code 
app.use((req,resp,next)=>{
    resp.header('Access-Control-Allow-Origin','*')
    resp.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if(req.method==='OPTIONS'){
        resp.header('Acess-Control-Allow-Methods','PUT, POST, PATCH, DELETE, GET ');
        return resp.status(200).json({ });
    }
    next();
})

mongoose.set('strictQuery', true);
mongoose.connect(`mongodb+srv://${process.env.uname}:${process.env.passwd}@cluster0.4ifhvrw.mongodb.net/ecommers?retryWrites=true&w=majority`)
.then(()=>{
    console.log("Database successfully connected");
})
.catch(()=>{
    console.log("Something went wrong while connecting with database");
})

app.use('/products',produtRoute);
app.use('/orders',orderRoute);
app.use('/user',signupRoute);


app.use((req,resp,next)=>{
    const error = new Error('Not Found');
    error.status = 404;
    next(error);

})

app.use((error,req,resp,next)=>{
    resp.status(error.status || 500)
    resp.json({
        error:{
            message:error.message, 
            code:error.status
        }
    })
})


module.exports = app;