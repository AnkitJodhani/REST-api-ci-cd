const path = require('path');
const fs = require('fs')



test('main server.js file exists', () => {
    const filePath = path.join(__dirname, "..","server.js")
    expect(fs.existsSync(filePath)).toBeTruthy();
  });
  
  test('Dockerfile exists', () => {
    const filePath = path.join(__dirname, "..", "Dockerfile")
    expect(fs.existsSync(filePath)).toBeTruthy();
  });
  
  test('.gitignore file exists', () => {
    const filePath = path.join(__dirname, "..", ".gitignore")
    expect(fs.existsSync(filePath)).toBeTruthy();
  });
