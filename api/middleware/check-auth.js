
const jwt = require('jsonwebtoken');

module.exports = (req,resp,next)=>{
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decode = jwt.verify(token, process.env.jwt_key);
        req.userData = decode;
        next();
    }
    catch(err){
        return resp.status(401).json({
            message:"Auth failed "
        });
    }
}



