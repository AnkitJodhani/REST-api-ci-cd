
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

const User = require('../models/user');
const userController = require('../controller/user');


// to create new user
router.post('/signup', userController.signup);


// use to login into system 
router.post('/login', userController.login);

// use to delete user
router.delete('/:userId', checkAuth, userController.delete_user)

module.exports = router;
