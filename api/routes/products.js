
const express = require('express');
const checkAuth = require('../middleware/check-auth');


const router = express.Router();

const productController = require('../controller/product');

// to get information about all the product
router.get('/', productController.get_all_product);


// to add new product into database
router.post('/',checkAuth, productController.add_product);



// to get information about only one product 
router.get('/:productId', productController.get_product);


// to update particular product information
router.patch('/:productId', checkAuth, productController.edit_product);


// to delete particular product 
router.delete('/:productId',checkAuth, productController.delete_product);



module.exports = router;