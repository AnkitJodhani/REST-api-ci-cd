
const express = require('express');
const checkAuth = require('../middleware/check-auth');
const router = express.Router();
const orderController = require('../controller/order');

// to get information about all the order
router.get('/', checkAuth, orderController.get_all_orders);


// to add new order into database
router.post('/',checkAuth, orderController.place_order);



// to get information about only one order 
router.get('/:orderId', checkAuth, orderController.get_order);
 

// to edit particular order information
router.patch('/:productId',checkAuth, orderController.edit_order);


// to delete particular order 
router.delete('/:orderId',checkAuth, orderController.delete_order);



module.exports = router;