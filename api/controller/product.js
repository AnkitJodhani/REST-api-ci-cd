
const mongoose = require('mongoose');
const Product = require('../models/product');



exports.get_all_product = (req, resp, next) => {
    const products = Product.find()
        .select('name price  _id')
        .then((result) => {
            // result is Array of products 
            if (result.length != 0) {
                const resposne = {
                    count: result.length,
                    products: result.map((x) => {
                        return {
                            name: x.name,
                            price: x.price,
                            _id: x._id,
                            request: {
                                type: 'GET',
                                url: `http://localhost:3500/products/${x._id}`
                            }

                        }
                    })
                }
                resp.status(200).json(resposne);
            }
            else {
                resp.status(200).json({
                    message: "There is no Product "
                })
            }
        })
        .catch((err) => {
            resp.status(500).json({
                error: err
            })
        })
}

exports.add_product = (req, resp, next) => {
    const product = new Product({
        name: req.body.name,
        price: req.body.price
    })
    product.save()
        .then((result) => {
            const response = {
                name: result.name,
                price: result.price,
                _id: result._id,
                request: {
                    type: 'GET',
                    url: `http://localhost:3500/products/${result._id}`
                }
            }
            resp.status(200).json({
                message: "Product inserted successfully",
                product: response
            })
        })
        .catch((err) => {
            console.log("errror occuring herer");
            resp.status(500).json({
                error: err
            })
        })
}

exports.get_product = (req, resp, next) => {
    const id = req.params.productId;
    const product = Product.findById(id)
        .select('_id name price')
        .then((result) => {
            const response = {
                name:result.name,
                price:result.price,
                _id:result._id,
                request:{
                    desc:"Click below link and get all product information",
                    type:'GET',
                    url:'http://localhost:3500/products'
                }
            }
            // result is just one object
            resp.status(200).json({
                message: "There you go",
                product: response
            })
        })
        .catch((err) => {
            resp.status(404).json({
                error: err
            })
        })

}

exports.edit_product = (req, resp, next) => {
    const id = req.params.productId;
    const updateop = {};
    for (const op of req.body) {
        updateop[op.propName] = op.value
    }
    const product = Product.updateOne(
        { _id: id },
        { $set: updateop }
    )
    .then((result) => {
        const response = {
            message:"Your product is updated successfully",
            request:{
                type:'GET',
                desc:'click below link to see all information about the product',
                url:`http://localhost:3500/products/${id}`
            }
        }
        if (result.matchedCount != 0) {
            console.log(result);
            resp.status(200).json(response)
        }
        else {
            resp.status(404).json({
                message: "Specified product is not avilable pleaase correct it."
            })
        }
    })
    .catch((err) => {
        resp.status(500).json({
            error: err
        })
    })

}

exports.delete_product = (req, resp, next) => {
    const id = req.params.productId;
    const product = Product.remove({ _id: id })
        .then((result) => {
            const response = {
                message:"Product deleted successfully",
                request:{
                    type:'POST', 
                    desc:"Click below link to create new product",
                    url:'http://localhost:3000/products/'
                }
            }
            if (result.deletedCount != 0) {
                resp.status(200).json(response)
            }
            else {
                resp.status(404).json({
                    message: "Your specifide product not exist in database"
                })
            }
        })
        .catch((err) => {
            resp.status(404).json({
                error: err
            })
        })
}

