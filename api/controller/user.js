const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');


// use to add new user
exports.signup = (req, resp, next) => {
    User.find({ email: req.body.email })
        .exec()
        .then((user) => {
            if (user.length >= 1) {
                return resp.status(409).json({
                    message: "Account already exist with same email id "
                })
            }
            else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return resp.status(500).json({
                            error: err
                        })
                    }
                    else {
                        const user = new User({
                            email: req.body.email,
                            password: hash

                        })
                        user.save()
                            .then((result) => {
                                resp.status(201).json({
                                    message: "User created successfully. "
                                })
                            })
                            .catch((err) => {
                                resp.status(500).json({
                                    error: err
                                })
                            })
                    }
                })

            }
        })
};

// use to login in system
exports.login = (req, resp, next) => {
    User.find({ email: req.body.email })
        .then((user) => {
            if (user.length < 1) {
                resp.status(401).json({
                    message: "Auth faild."
                })
            }
            else {
                bcrypt.compare(req.body.password, user[0].password, (err, result) => {
                    if (err) {
                        resp.status(401).json({
                            message: "Auth faild."
                        })
                    }
                    if (result) {
                        const token = jwt.sign(
                            {
                                email: user[0].email,
                                userId: user[0]._id
                            },
                            process.env.jwt_key,
                            {
                                expiresIn: "1h"
                            }
                        );
                        return resp.status (200).json({
                            message: 'Auth successfull',
                            token:token
                        })
                    }
                })
            }
        })
        .catch((err) => {
            resp.status(500).json({
                error: err
            })
        })
};

// use to delete user
exports.delete_user = (req, resp, next) => {
    User.remove({ _id: req.params.userId })
        .then((result) => {
            if (result.deletedCount != 0) {
                resp.status(200).json({
                    message: "User deleted successfully."
                })
            }
            else {
                resp.status(200).json({
                    message: "Please specify correct userid"
                })
            }
        })
        .catch((err) => {
            resp.status(500).json({
                error: err
            })
        })

}