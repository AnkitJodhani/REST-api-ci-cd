const mongoose = require('mongoose');
const Order = require('../models/order');
const Product = require('../models/product');

exports.get_all_orders = (req, resp, next) => {
    const orders = Order.find()
        .populate('product', 'name _id')
        .then((result) => {
            const response = result.map((x) => {
                return {
                    _id: x._id,
                    product: x.product,
                    quantity: x.quantity,
                    request: {
                        type: 'GET',
                        desc: 'use below link to know about a order',
                        url: `http://localhost:3500/orders/${x._id}`
                    }
                }
            })
            resp.status(200).json({
                count: response.length,
                order: response
            })
        })
        .catch((err) => {
            resp.status(500).json({
                error: err
            })
        })
}

exports.place_order = (req, resp, next) => {
    Product.findById(req.body.productId)
        .then((product) => {
            if (!product) {
                resp.status(404).json({
                    message: 'Product not found'
                })
            }
            const order = new Order({
                product: req.body.productId,
                quantity: req.body.quantity
            })
            return order.save()
        })
        .then((result) => {
            const response = {
                productId: result.product,
                quantity: result.quantity,
                request_order: {
                    type: 'GET',
                    desc: 'Get the information about the order.',
                    url: `http://localhost:3500/orders/${result._id}`
                },
                request_product: {
                    type: 'GET',
                    desc: 'Get the information about the product',
                    url: `http://localhost:3500/products/${result.product.toString()}`
                }
            }
            resp.status(200).json({
                message: "Product added successfully",
                order: response
            })

        })
        .catch((err) => {
            resp.status(500).json({
                error: err
            })
        })
}

exports.get_order = (req, resp, next) => {
    const id = req.params.orderId;
    const order = Order.findById(id)
        .populate('product', 'name _id')
        .select(' _id product quantity')
        .then((result) => {
            resp.status(200).json({
                message: "Here yuu go!!",
                order: result
            })
        })
        .catch((err) => {
            error: err
        })


}

exports.edit_order = (req, resp, next) => {
    const id = req.params.productId;
    const updateop = {}
    for (op of req.body) {
        updateop[op.propName] = op.value
    }
    const order = Order.updateOne(
        { _id: id },
        { $set: updateop }
    )
        .then((result) => {
            if (result.matchedCount != 0) {
                const response = {
                    message: "Product updated successfully",
                    request: {
                        type: "GET",
                        desc: "go throught below link to know more about order",
                        url: `http://localhost:3500/orders/${id}`
                    }
                }
                resp.status(200).json(response)
            }
            else {
                resp.status(500).json({
                    message: "Specified order not in the list"
                })
            }
        })
        .catch((err) => {
            resp.status(500).json({
                error: err
            })
        })

}

exports.delete_order = (req, resp, next) => {
    const id = req.params.orderId;
    const order = Order.remove({ _id: id })
        .then((result) => {
            if (result.deletedCount != 0) {
                resp.status(200).json({
                    message: "Product deleted successfully",
                    orderid: id,
                    request: {
                        type: 'POST',
                        desc: 'go throught below link to add new order',
                        url: "http://localhost:3500/orders/"
                    }
                })
            }
            else {
                resp.status(200).json({
                    message: "Specified product not present in order list.."
                })
            }
        })
        .catch((err) => {
            error: err
        })
}

